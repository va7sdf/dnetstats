#!/usr/bin/env python
# Simple script to get overall and yesterday's rank, percentile, blocks,
# blocks/sec, keys, keys/sec, and time working for a participant of the
# distributed.net RC5-72 contest.  Then append these to a cvs file.

import csv
import os
import re
import urllib
from datetime import datetime

dnet_id    = '541835'
statsfile  = '%s_stats.csv' % (dnet_id)
pagelink   = 'http://stats.distributed.net/participant/psummary.php?project_id=8&id=%s' % (dnet_id)

lastupdate_regex   = ('lastupdate.*?(\d{2})-([A-Z][a-z]{2})-(\d{4})')
rank_regex         = ('Rank:.*?(\d+)(?=(?=<span)(.*?"([+-]">\d+))|(\s+)).*?(\d+)(?=(?=<span)(.*?"([+-]">\d+))|(\s+))')
percentile_regex   = ('Percentile:.*?([\d.]+).*?([\d.]+)')
blocks_regex       = ('Blocks:.*?([\d,]+).*?([\d,]+)')
blockspersec_regex = ('Blocks/sec:.*?([\d.]+).*?([\d.]+)')
keys_regex         = ('Keys:.*?([\d,]+).*?([\d,]+)')
keyspersec_regex   = ('Keys/sec:.*?([\d,]+).*?([\d,]+)')
timeworking_regex  = ('(\d+)(?= days)')


# Read and parse page for stats
userpage = urllib.urlopen(pagelink).read()

lastupdate_stats   = re.search(lastupdate_regex, userpage, re.MULTILINE|re.DOTALL)
rank_stats         = re.search(rank_regex, userpage, re.MULTILINE|re.DOTALL)
percentile_stats   = re.search(percentile_regex, userpage, re.MULTILINE|re.DOTALL)
blocks_stats       = re.search(blocks_regex, userpage, re.MULTILINE|re.DOTALL)
blockspersec_stats = re.search(blockspersec_regex, userpage, re.MULTILINE|re.DOTALL)
keys_stats         = re.search(keys_regex, userpage, re.MULTILINE|re.DOTALL)
keyspersec_stats   = re.search(keyspersec_regex, userpage, re.MULTILINE|re.DOTALL)
timeworking_stats  = re.search(timeworking_regex, userpage, re.MULTILINE|re.DOTALL)

# Append stats to CSV
csvfile  = open(statsfile, 'ab')
csvwriter = csv.writer(csvfile)

if os.stat(statsfile).st_size == 0:
    csvwriter.writerow(['DATE',
        'RANK_ALL','RANK_ALL_CNG',
        'RANK_DAY','RANK_DAY_CNG',
        'PERCENTILE_ALL','PERCENTILE_DAY',
        'BLOCKS_ALL','BLOCKS_DAY',
        'BLOCKS-PER-SEC_ALL','BLOCKS-PER-SEC_DAY',
        'KEYS_ALL','KEYS_DAY',
        'KEYS-PER-SEC_ALL','KEYS-PER-SEC_DAY',
        'DAYS_WORKING']);

if rank_stats.group(3):
    overallupdown = rank_stats.group(3).replace('">','')
else:
    overallupdown = '0'

if rank_stats.group(7):
    yesterdayupdown = rank_stats.group(7).replace('">','')
else:
    yesterdayupdown = '0'

csvwriter.writerow(('-'.join(lastupdate_stats.groups()[::-1]) + ' 23:59',)
        + (rank_stats.group(1), overallupdown, rank_stats.group(5), yesterdayupdown)
        + percentile_stats.groups()
        + blocks_stats.groups()
        + blockspersec_stats.groups()
        + keys_stats.groups()
        + keyspersec_stats.groups()
        + timeworking_stats.groups())
csvfile.close()
