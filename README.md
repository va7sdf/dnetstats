# dnetcstats
Simple script to get overall and yesterday's rank, percentile, blocks, blocks/sec, keys, keys/sec, and time working for a participant of the distributed.net RC5-72 contest.  Then append these to a cvs file.
